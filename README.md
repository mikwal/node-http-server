# NodeJS HTTP server

Lightweight http server for NodeJS micro services with optional static resources.

Listening port can be specified by `options.port` argument, in `package.json/httpServer/port` or by setting the environment variable `PORT`. (the latter takes precedence).  

A health check URL can be specified by `options.healthCheckUrl` argument or in `package.json/httpServer/healthCheckUrl`.   

Static resources can be specified in `options.assets` argument or in `package.json/httpServer/assets`. Keys in the map is the physical file or directory and values are the relative URL.  

A relative URL in argument `options.assetsManifestPath` or `package.json/httpServer/assetsManifestPath` to expose a list of all static resources.  

Usage:  
```JavaScript
let { runHttpServer } = require('@mikwal/node-http-server');

runHttpServer((request, response) => {
    response.end('Echo for URL=' + request.url);
});

```