let http = require('http');
let path = require('path');
let fs = require('fs');
let crypto = require('crypto');
let log = require('@mikwal/node-console-logger').createLogger('http-server');
let { healthCheckHandler } = require('./health-check');
let { assetsHandler, assetsManifestHandler, getAssetsManifest } = require('./assets');

module.exports = { runHttpServer, loadHttpServerOptions, assetsHandler, getAssetsManifest };


function runHttpServer(callback, options = loadHttpServerOptions()) {
    
    if (typeof callback !== 'function') {
        throw new Error(`Parameter 'callback' must be a function.`);
    }

    let assetsManifest = getAssetsManifest(options);
    let requestListener = composeHandlerPipeline([
        options.healthCheckPath && healthCheckHandler(options.healthCheckPath),
        assetsManifest.size && assetsHandler(assetsManifest),
        assetsManifest.path && assetsManifestHandler(assetsManifest),
        callback
    ]);

    try {
        log.info('Starting server');
    
        let connections = new Map();
        let server = http.
            createServer(requestListener).
            on('listening', () => log.info(`Server started, listening on: ${JSON.stringify(server.address())}`)).
            on('error', error => log.error('Error running server', error)).
            on('close', () => log.info('Server stopped')).
            on('connection', socket => {
                let id = uuid();
                connections.set(id, socket);
                socket.on('close', () => connections.delete(id));
            }).
            listen(process.env.PORT || options.port || '80');

        process.once('SIGINT', () => {
            log.info('Stopping server');
            process.once('SIGINT', () => process.exit(130));
            server.close(() => process.exit(130));
            connections.forEach(socket => socket.destroy());
        });

        return server;
    }
    catch (error) {
        log.error('Error starting server', error);
        process.exit(1);
    }
}

function composeHandlerPipeline(handlers) {
    return handlers.reverse().filter(Boolean).reduce((f, g) => (...args) => g(...args, () => f(...args)), );
}

function loadHttpServerOptions(dir = process.cwd()) {
    let packagePath = path.join(dir, 'package.json');
    return (
        (fs.existsSync(packagePath) && JSON.parse(fs.readFileSync(packagePath, 'utf8')).httpServer) ||
        (dir !== path.parse(dir).root && loadHttpServerOptions(path.dirname(dir))) ||
        {}
    );
}

function uuid() {
    return crypto.randomBytes(16).toString('hex');
}