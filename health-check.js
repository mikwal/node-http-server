let url = require('url');

module.exports = { healthCheckHandler };


function healthCheckHandler(healthCheckPath) {
    return (request, response, next) => {
        let isGet = request.method === 'GET';
        let isGetOrHead = isGet || request.method === 'HEAD';
        if (isGetOrHead && healthCheckPath === decodeURIComponent(url.parse(request.url).pathname)) {
            response.writeHead(200, {
                'Content-Type': 'text/plain',
                'Cache-Control': 'no-cache'
            });
            response.end(isGet ? 'OK' : undefined);
            return;
        }
        next();
    };
}
