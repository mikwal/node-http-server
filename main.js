#!/usr/bin/env node

let { runHttpServer } = require('./index.js');

runHttpServer((request, response) => {
    response.statusCode = 404;
    response.end();
});