import { Server, ServerResponse, IncomingMessage } from 'http';

export type HttpServerCallback = (request: IncomingMessage, response: ServerResponse) => void;

export type HttpServerOptions = {
    port?: number | string;
    healthCheckPath?: string;
} & AssetsManifestOptions;

export declare function loadHttpServerOptions(dir?: string): HttpServerOptions;
export declare function runHttpServer(callback: HttpServerCallback, options?: HttpServerOptions): Server;


export type AssetsManifestOptions = {
    assets?: Record<string, string>;
    assetsManifestPath?: string;
    includeAssetHash?: boolean;
    rewriteAssetPaths?: Record<string, string>;
    headers?: Record<string, string | string[]>;
};

export type AssetsManifestEntry = {
    physicalFile: string;
    size: number;
    lastModified: number;
    contentType: string;
    hash?: string;
};

export type AssetsManifest = {
    get(key: string): AssetsManifestEntry | undefined;
    readonly path: string;
    readonly includeHash: boolean;
    readonly headers: Record<string, string | string[]>;
};

export type AssetsManifestHandler = (request: IncomingMessage, response: ServerResponse, next: () => void) => void;

export declare function getAssetsManifest(options?: AssetsManifestOptions): AssetsManifest;
export declare function assetsManifestHandler(assetsManifest: AssetsManifest): AssetsManifestHandler;