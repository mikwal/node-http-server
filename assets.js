let url = require('url');
let path = require('path');
let fs = require('fs');
let crypto = require('crypto');

module.exports = { assetsHandler, assetsManifestHandler, getAssetsManifest };

const RANGE_HEADER_REG_EXP = /^ *bytes=((?:\d+-|-\d+|\d+-\d+)(?:, *(?:\d+-|-\d+|\d+-\d+))*)$/;
const ASSET_NAME_WITH_HASH_REG_EXP = /^(.*[^\/])\.([0-9a-fA-F]{40})(\.[^\/\.]*)?$/;

function assetsHandler(assetsManifest) {
    return (request, response, next) => {
        let isGet = request.method === 'GET';
        let isGetOrHead = isGet || request.method === 'HEAD';
        if (isGetOrHead) {
            let pathname = decodeURIComponent(url.parse(request.url).pathname), hash;
            let asset = assetsManifest.get(pathname);
            let cacheControl = 'no-cache';
            if (!asset && assetsManifest.includesHash) {
                [pathname, hash] = hashFromAssetPath(pathname);
                asset = hash && assetsManifest.get(pathname);
                cacheControl = 'maxAge=31536000';
            }
            if (asset && (!hash || hash === asset.hash)) {
                let range = request.headers.range && parseRangeHeader(request.headers.range, asset.size);
                if (range && range.error) {
                    response.writeHead(416, { 'Content-Range': `bytes */${asset.size}` });
                    response.end();
                    return;
                }
                response.writeHead(range ? 206 : 200, {
                    ...assetsManifest.headers,
                    'Accept-Ranges': 'bytes',
                    'Content-Type': asset.contentType,
                    'Content-Length': (range && (range.end - range.start + 1)) || asset.size,
                    ...(range && { 'Content-Range': `bytes ${range.start}-${range.end}/${asset.size}` }),
                    'Cache-Control': cacheControl,
                    'Last-Modified': new Date(asset.lastModified).toGMTString()
                });
                if (isGet) {
                    fs.createReadStream(asset.physicalFile, { ...range }).pipe(response);
                }
                else {
                    response.end();
                }
                return;
            }
        }
        next();
    };
}

function assetsManifestHandler(assetsManifest) {
    return (request, response, next) => {
        let isGet = request.method === 'GET';
        let isGetOrHead = isGet || request.method === 'HEAD';
        if (isGetOrHead && assetsManifest.path === decodeURIComponent(url.parse(request.url).pathname)) {
            response.writeHead(200, {
                ...assetsManifest.headers,
                'Content-Type': 'text/plain',
                'Cache-Control': 'no-cache'
            });
            response.end(isGet
                ? [...assetsManifest].
                    sort((a, b) => a[0].localeCompare(b[0])).
                    map(a => [
                        a[0],
                        a[1].size,
                        a[1].lastModified,
                        a[1].contentType,
                        ...(a[1].hash ? [a[1].hash] : [])
                    ].join('\t')).
                    join('\n')
                : undefined);
            return;
        }
        next();
    };
}

function getAssetsManifest(options = {}) {

    let mimeTypes = loadMimeTypesDatabase(__dirname + '/mime.types');
    let assetsManifest = Object.assign(new Map(), {
        path: (options.assetsManifestPath && String(options.assetsManifestPath)) || undefined, 
        includesHash: Boolean(options.includeAssetHash),
        headers: { ...options.headers }
    });

    let rewritePaths = Object.entries(options.rewriteAssetPaths || {}).map(([from, to]) => [new RegExp(from), to]);

    for (let [fileOrDir, pathname] of Object.entries(options.assets || {})) {
        addRecursive(fileOrDir, pathname, []);
    }
    return assetsManifest;

    function addRecursive(fileOrDir, pathname, rewrite) {
        let stat = fs.statSync(fileOrDir);
        if (stat.isDirectory()) {
            for (let nestedFileOrDir of fs.readdirSync(fileOrDir)) {
                if (!nestedFileOrDir.startsWith('.')) {
                    addRecursive(
                        path.join(fileOrDir, nestedFileOrDir),
                        pathname + (pathname.endsWith('/') ? '' : '/') + nestedFileOrDir,
                        rewritePaths
                    );
                }
            }
        }
        else {
            if (!assetsManifest.has(fileOrDir)) {
                for (let [from, to] of rewrite) {
                    pathname = pathname.replace(from, to);
                }
                assetsManifest.set(pathname, {
                    physicalFile: fileOrDir,
                    lastModified: Math.trunc(stat.mtimeMs),
                    contentType: mimeTypes.get(path.extname(fileOrDir).substr(1)) || 'application/octet-stream',
                    size: Math.trunc(stat.size),
                    ...(assetsManifest.includesHash && { hash: hashFileSync(fileOrDir) })
                });
            }
        }
    }
}

function hashFileSync(path) {
    let fd = fs.openSync(path, 'r');
    try {
        let hash = crypto.createHash('sha1');
        let buffer = Buffer.alloc(4096), count;
        while ((count = fs.readSync(fd, buffer, 0, buffer.length)) > 0) {
            hash.update(buffer.slice(0, count));
        }
        return hash.digest('hex');
    }
    finally {
        fs.closeSync(fd);
    }
}

function hashFromAssetPath(pathname) {
    let match = ASSET_NAME_WITH_HASH_REG_EXP.exec(pathname);
    return match
        ? [match[1] + (match[3] || ''), match[2]]
        : [pathname];
}

function loadMimeTypesDatabase(file) {
    let map = new Map();
    for (let line of fs.readFileSync(file, 'utf8').split('\n')) {
        let parts = line.trim().split(/\s+/g);
        for (let extname of parts.slice(1)) {
            map.set(extname, parts[0]);
        }
    }
    return map;
}

function parseRangeHeader(range, size) {
    let match = RANGE_HEADER_REG_EXP.exec(range);
    let parsed = match &&
        match[1].split(',').map(r =>
            r.split('-').map(n => parseInt(n))).map(([start, end]) =>
                Number.isNaN(start)
                    ? { start: size - end, end: size - 1 }
                    : { start, end: Number.isNaN(end) ? size - 1 : end });

    return parsed && parsed.every(({ start, end }) => start >= 0 && start <= end && end < size)
        ? (parsed.length === 1 && parsed[0]) || undefined
        : { error: true };
}